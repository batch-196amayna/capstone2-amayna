const mongoose = require("mongoose");
const productsSchema = new mongoose.Schema({

	name:{
		type:String,
		required:[true, "Product Name is required"]
	},
	description:{
		type:String,
		required:[true, "Product Description is required"]
	},
	price:{
		type:Number,
		required:[true, "product Price is required"]
	},
	isActive:{
		type:Boolean,
		default: true
	},
	createdOn:{
		type:Date,
		default: new Date()
	},
	order:[{
			orderId:{
				type:String,
				required:[true, "Order ID is required"]
			},
			userId:{
				type:String,
				required:[true, "User ID is required"]
			},
			purchasedOn:{
				type:Date,
				default: new Date()
			},
			quantity:{
					type:Number,
					required:[true, "Quantity is required"]
			}
		}]
		
})
module.exports = mongoose.model("Products", productsSchema);