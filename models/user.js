const mongoose = require("mongoose");
const usersSchema = new mongoose.Schema({

	firstName:{
		type:String,
		required:[true, "First Name is required"]
	},
	lastName:{
		type:String,
		required:[true, "Last Name is required"]
	},
	email:{
		type:String,
		required:[true, "Email is required"]
	},
	password:{
		type:String,
		required:[true, "Pasword is required"]
	},
	mobileNo:{
		type:String,
		required:[true, "Mobile Number is required"]
	},
	isAdmin:{
		type:Boolean,
		default: false
	},
	order:[{
			totalAmount:{
				type:Number,
				required:[true, "Total Amount"]
			},
			purchasedOn:{
				type:Date,
				default: new Date()
			},
			products:[{
				productId:{
					type:String,
					required:[true, "Product ID is required"]
				},
			quantity:{
					type:Number,
					required:[true, "Quantity is required"]
				}
			}]
		}]

})
module.exports = mongoose.model("Users", usersSchema);