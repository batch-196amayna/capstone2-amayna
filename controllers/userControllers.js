const User = require("../models/User")

const Product = require("../models/Product")

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res)=>{
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw)

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.userDetails = (req,res)=>{
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.userLogin = (req,res)=>{
	console.log(req.body);
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send({message:"No User Found"})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message:"Incorrect Password"});
			}
		}
	})
	.catch(error => res.send(error))
}


module.exports.setUser = (req,res)=>{
	console.log(req.params.userId)

	let update = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.userOrder = async (req,res)=>{
	if(req.user.isAdmin){
		return res.send({message:"You can't take order!"});
	}
	
	let isUserUpdated = await User.findById(req.user.id).then(user =>{
		user.order.push(req.body)
		//console.log(user.order[user.order.length-1])
		let newOrder = user.order[user.order.length-1]
		let orderId = newOrder.id
		//console.log(req.body.products)

		let reqBodyProducts = req.body.products
		reqBodyProducts.forEach(function(productDetails){
			//console.log(productDetails)
			Product.findById(productDetails.productId).then(product => {
			//console.log(product)
			let orderDetails = {
				orderId: orderId,
				quantity: productDetails.quantity,
				userId: req.user.id
			}
			product.order.push(orderDetails);
			product.save().then().catch(err=>err.message);
			})
		})
		return user.save().then(user=>true).catch(err=>err.message)
	})

	if(isUserUpdated !== true){
		return res.send({message:isUserUpdated});
	} else {
		return res.send({message: "Thank you for ordering"})
	}
}

module.exports.userOrders = (req,res)=>{
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.allOrders = (req,res)=>{
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}