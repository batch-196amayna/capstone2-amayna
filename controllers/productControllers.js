const Product = require("../models/Product");

module.exports.active = (req,res)=>{
	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.addProduct = (req,res)=>{
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});
	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleProduct = (req,res)=>{
	//console.log(req.params)
	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateProduct = (req,res)=>{
	//console.log(req.params.courseId)
	//console.log(req.body)
	
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.archiveProduct = (req,res)=>{

	//console.log(req.params.courseId)

	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
