const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const userControllers = require("../controllers/userControllers");
console.log(userControllers);

router.post("/", userControllers.registerUser)

router.get("/userDetails", verify, userControllers.userDetails)

router.post("/login", userControllers.userLogin)

router.put("/setUser/:userId", verify, verifyAdmin, userControllers.setUser)

router.post("/order", verify, userControllers.userOrder)

router.get("/getUserOrder", verify, userControllers.userOrders)

router.get("/getAllOrder", verify, verifyAdmin, userControllers.allOrders)

module.exports = router;
